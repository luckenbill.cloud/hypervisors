#!/bin/sh -e

HOSTNAME="$1"
if [ -z "$HOSTNAME" ]; then
	echo "usage: $0 hostname"
	exit 1
fi

cleanup() {
	rm -rf "${tmp}"
}

tmp="$(mktemp -d)"
trap cleanup EXIT

installfile() {
  install -o ${3} -g ${3} -m 0644 -D -t "${tmp}/${2}" ${1}
}

rc_add() {
	mkdir -p "${tmp}/etc/runlevels/${2}"
	ln -sf "/etc/init.d/${1}" "${tmp}/etc/runlevels/${2}/${1}"
}


useradd $USER1_NAME -u 1000 -U

#----- File Setup

#-- Hostname
tee hostname << EOF > /dev/null
hyp0
EOF

installfile hostname etc/ root

#-- Network Interfaces
tee interfaces << EOF > /dev/null
auto lo eth0 eth1 eth2 eth3

iface lo inet loopback

iface eth0 inet dhcp
    hwaddress ether 90:B1:1C:2C:CD:1F

iface eth1 inet dhcp
    hwaddress ether 90:B1:1C:2C:CD:20

iface eth2 inet dhcp
    hwaddress ether 90:B1:1C:2C:CD:21

iface eth3 inet dhcp
    hwaddress ether 90:B1:1C:2C:CD:22
EOF

installfile interfaces etc/network/ root

#-- Apk World
tee world << EOF > /dev/null
alpine-base
alpine-mirrors
busybox
kbd-bkeymaps
chrony
e2fsprogs
haveged
network-extras
openssl
openssh
tzdata
ethtool
lvm2
mdadm
multipath-tools
openvswitch
rng-tools
sfdisk
bridge
libvirt
libvirt-daemon
qemu-img
qemu-system-x86_64
util-linux
EOF

installfile world etc/apk/ root

#-- Apk Repositories
tee repositories << EOF > /dev/null
http://dl-cdn.alpinelinux.org/alpine/v3.11/community
http://dl-cdn.alpinelinux.org/alpine/v3.11/main
EOF

installfile repositories etc/apk/ root

#-- SSH Authorized Keys
tee authorized_keys << EOF > /dev/null
$USER1_PUBLIC_KEY1
EOF

installfile authorized_keys home/$USER1_NAME/.ssh/ $USER1_NAME

tee authorized_keys << EOF > /dev/null
$ROOT_PUBLIC_KEY
EOF

installfile authorized_keys root/.ssh/ root

#-- SSH Deamon Config
tee sshd_config << EOF > /dev/null
AuthenticationMethods publickey
PasswordAuthentication no
Banner /etc/ssh/banner
EOF

installfile sshd_config etc/ssh/ root

#-- KVM Kernel Modules
tee kvm.conf << EOF > /dev/null
kvm
kvm_intel
dm-mod
EOF

installfile kvm.conf etc/modules-load.d root

#-- Groups
tee group << EOF > /dev/null
root:x:0:root
bin:x:1:root,bin,daemon
daemon:x:2:root,bin,daemon
sys:x:3:root,bin,adm
adm:x:4:root,adm,daemon
tty:x:5:
disk:x:6:root,adm
lp:x:7:lp
mem:x:8:
kmem:x:9:
wheel:x:10:root
floppy:x:11:root
mail:x:12:mail
news:x:13:news
uucp:x:14:uucp
man:x:15:man
cron:x:16:cron
console:x:17:
audio:x:18:
cdrom:x:19:
dialout:x:20:root
ftp:x:21:
sshd:x:22:
input:x:23:
at:x:25:at
tape:x:26:root
video:x:27:root
netdev:x:28:
readproc:x:30:
squid:x:31:squid
xfs:x:33:xfs
kvm:x:34:kvm,$USER1_NAME
games:x:35:
shadow:x:42:
postgres:x:70:
cdrw:x:80:
usb:x:85:
vpopmail:x:89:
users:x:100:games
ntp:x:123:
nofiles:x:200:
smmsp:x:209:smmsp
locate:x:245:
abuild:x:300:
utmp:x:406:
ping:x:999:
nogroup:x:65533:
nobody:x:65534:
libvirt:x:101:$USER1_NAME
dnsmasq:x:102:dnsmasq
qemu:x:36:$USER1_NAME
$USER1_NAME:x:1000:$USER1_NAME
EOF

tee group- << EOF > /dev/null
root:x:0:root
bin:x:1:root,bin,daemon
daemon:x:2:root,bin,daemon
sys:x:3:root,bin,adm
adm:x:4:root,adm,daemon
tty:x:5:
disk:x:6:root,adm
lp:x:7:lp
mem:x:8:
kmem:x:9:
wheel:x:10:root
floppy:x:11:root
mail:x:12:mail
news:x:13:news
uucp:x:14:uucp
man:x:15:man
cron:x:16:cron
console:x:17:
audio:x:18:
cdrom:x:19:
dialout:x:20:root
ftp:x:21:
sshd:x:22:
input:x:23:
at:x:25:at
tape:x:26:root
video:x:27:root
netdev:x:28:
readproc:x:30:
squid:x:31:squid
xfs:x:33:xfs
kvm:x:34:kvm
games:x:35:
shadow:x:42:
postgres:x:70:
cdrw:x:80:
usb:x:85:
vpopmail:x:89:
users:x:100:games
ntp:x:123:
nofiles:x:200:
smmsp:x:209:smmsp
locate:x:245:
abuild:x:300:
utmp:x:406:
ping:x:999:
nogroup:x:65533:
nobody:x:65534:
libvirt:x:101:
dnsmasq:x:102:dnsmasq
qemu:x:36:
EOF

installfile group etc/ root
installfile group- etc/ root

#-- Shadow
tee shadow << EOF > /dev/null
root:*::0:::::
bin:!::0:::::
daemon:!::0:::::
adm:!::0:::::
lp:!::0:::::
sync:!::0:::::
shutdown:!::0:::::
halt:!::0:::::
mail:!::0:::::
news:!::0:::::
uucp:!::0:::::
operator:!::0:::::
man:!::0:::::
postmaster:!::0:::::
cron:!::0:::::
ftp:!::0:::::
sshd:!::0:::::
at:!::0:::::
squid:!::0:::::
xfs:!::0:::::
games:!::0:::::
postgres:!::0:::::
cyrus:!::0:::::
vpopmail:!::0:::::
ntp:!::0:::::
smmsp:!::0:::::
guest:!::0:::::
nobody:!::0:::::
dnsmasq:!:18149:0:99999:7:::
$USER1_NAME:*:18149:0:99999:7:::
EOF

tee shadow- << EOF > /dev/null
root:::0:::::
bin:!::0:::::
daemon:!::0:::::
adm:!::0:::::
lp:!::0:::::
sync:!::0:::::
shutdown:!::0:::::
halt:!::0:::::
mail:!::0:::::
news:!::0:::::
uucp:!::0:::::
operator:!::0:::::
man:!::0:::::
postmaster:!::0:::::
cron:!::0:::::
ftp:!::0:::::
sshd:!::0:::::
at:!::0:::::
squid:!::0:::::
xfs:!::0:::::
games:!::0:::::
postgres:!::0:::::
cyrus:!::0:::::
vpopmail:!::0:::::
ntp:!::0:::::
smmsp:!::0:::::
guest:!::0:::::
nobody:!::0:::::
dnsmasq:!:18149:0:99999:7:::
$USER1_NAME:!:18149:0:99999:7:::
EOF

installfile shadow etc/ root
installfile shadow- etc/ root

#-- Passwd File
tee passwd << EOF > /dev/null
root:x:0:0:root:/root:/bin/ash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
news:x:9:13:news:/usr/lib/news:/sbin/nologin
uucp:x:10:14:uucp:/var/spool/uucppublic:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
man:x:13:15:man:/usr/man:/sbin/nologin
postmaster:x:14:12:postmaster:/var/spool/mail:/sbin/nologin
cron:x:16:16:cron:/var/spool/cron:/sbin/nologin
ftp:x:21:21::/var/lib/ftp:/sbin/nologin
sshd:x:22:22:sshd:/dev/null:/sbin/nologin
at:x:25:25:at:/var/spool/cron/atjobs:/sbin/nologin
squid:x:31:31:Squid:/var/cache/squid:/sbin/nologin
xfs:x:33:33:X Font Server:/etc/X11/fs:/sbin/nologin
games:x:35:35:games:/usr/games:/sbin/nologin
postgres:x:70:70::/var/lib/postgresql:/bin/sh
cyrus:x:85:12::/usr/cyrus:/sbin/nologin
vpopmail:x:89:89::/var/vpopmail:/sbin/nologin
ntp:x:123:123:NTP:/var/empty:/sbin/nologin
smmsp:x:209:209:smmsp:/var/spool/mqueue:/sbin/nologin
guest:x:405:100:guest:/dev/null:/sbin/nologin
nobody:x:65534:65534:nobody:/:/sbin/nologin
dnsmasq:x:100:102:dnsmasq:/dev/null:/sbin/nologin
$USER1_NAME:x:1000:36:Linux User,,,:/home/$USER1_NAME:/bin/ash
EOF

tee passwd- << EOF > /dev/null
root:x:0:0:root:/root:/bin/ash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
news:x:9:13:news:/usr/lib/news:/sbin/nologin
uucp:x:10:14:uucp:/var/spool/uucppublic:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
man:x:13:15:man:/usr/man:/sbin/nologin
postmaster:x:14:12:postmaster:/var/spool/mail:/sbin/nologin
cron:x:16:16:cron:/var/spool/cron:/sbin/nologin
ftp:x:21:21::/var/lib/ftp:/sbin/nologin
sshd:x:22:22:sshd:/dev/null:/sbin/nologin
at:x:25:25:at:/var/spool/cron/atjobs:/sbin/nologin
squid:x:31:31:Squid:/var/cache/squid:/sbin/nologin
xfs:x:33:33:X Font Server:/etc/X11/fs:/sbin/nologin
games:x:35:35:games:/usr/games:/sbin/nologin
postgres:x:70:70::/var/lib/postgresql:/bin/sh
cyrus:x:85:12::/usr/cyrus:/sbin/nologin
vpopmail:x:89:89::/var/vpopmail:/sbin/nologin
ntp:x:123:123:NTP:/var/empty:/sbin/nologin
smmsp:x:209:209:smmsp:/var/spool/mqueue:/sbin/nologin
guest:x:405:100:guest:/dev/null:/sbin/nologin
nobody:x:65534:65534:nobody:/:/sbin/nologin
dnsmasq:x:100:102:dnsmasq:/dev/null:/sbin/nologin
$USER1_NAME:x:1000:36:Linux User,,,:/home/$USER1_NAME:/bin/ash
EOF

installfile passwd etc/ root
installfile passwd- etc/ root

tee motd << EOF > /dev/null
This is the motd
EOF

installfile motd etc/ root

tee banner << EOF > /dev/null
This is the banner
EOF

installfile banner etc/ssh/ root


tee drive-mount << EOF > /dev/null
#!/sbin/openrc-run

depend() {
        after lvm
}

start() {
       ebegin "Setting up Logical Volume group vg0"
       vgchange -a y vg0 > /dev/null 2>&1
       vgchange -a y vg1 > /dev/null 2>&1
#&& swapon -e /dev/vg0/lv_swap
# && mount /dev/vg0/lv_var /var > /dev/null 2>&1

       eend $?
}

stop() {
     ebegin "Removing Logical Volume Group vg0"
#     swapoff /dev/vg0/lv_swap
# || umount /dev/vg0/lv_var
     vgchange -a n vg0 > /dev/null 2>&1
     vgchange -a n vg1 > /dev/null 2>&1

     eend $?
}
EOF

install -o root -g root -m 0755 -D -t $tmp/etc/init.d drive-mount

tee fstab << EOF > /dev/null
/dev/cdrom	/media/cdrom	iso9660	  noauto,ro  0 0
/dev/usbdisk	/media/usb	vfat	  noauto,ro  0 0
/dev/vg0/vol0   /var/lib/libvirt       ext4      defaults   0 0
/dev/vg0/vol1   /u00/iso               ext4      defaults   0 0
/dev/vg0/vol2   /etc/libvirt           ext4      defaults   0 0
/dev/vg1/vol0   /u01/cloud      ext4      defaults   0 0 
/dev/vg1/vol1   /u01/media      ext4      defaults   0 0
/dev/vg1/vol2   /u01/nfs        ext4      defaults   0 0
/dev/vg1/vol3   /u01/storage    ext4      defaults   0 0
EOF

installfile fstab etc/ root

#----- Service Setup
# sysinit
rc_add devfs sysinit
rc_add dmesg sysinit
rc_add mdev sysinit
rc_add hwdrivers sysinit
rc_add modloop sysinit

# default
rc_add libvirtd default
rc_add networking default
rc_add sshd default

# boot
rc_add hwclock boot
rc_add modules boot
rc_add sysctl boot
rc_add hostname boot
rc_add bootmisc boot
rc_add syslog boot
rc_add lvm boot
rc_add swap boot

#ln -sf /etc/init.d/drive-mount ${tmp}/etc/runlevels/boot/drive-mount

# shutdown
rc_add mount-ro shutdown
rc_add killprocs shutdown
rc_add savecache shutdown


#----- Package
#mkdir -p $tmp/u01/qemu
#mkdir -p $tmp/u01/iso
#mkdir -p $tmp/u02/cloud
#mkdir -p $tmp/u02/media
#mkdir -p $tmp/u02/nfs
#mkdir -p $tmp/u02/storage
ls -la


chown -R $USER1_NAME:$USER1_NAME $tmp/home/$USER1_NAME
tar -c -C "$tmp" etc root home | gzip -9n > $HOSTNAME.apkovl.tar.gz
