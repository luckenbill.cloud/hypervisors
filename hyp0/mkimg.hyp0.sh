build_hype0() {
    echo "section build"
#	  apk fetch --root "$APKROOT" --stdout xen-hypervisor | tar -C "$DESTDIR" -xz boot
}

section_hyp0() {
    echo "section hyp0"
#	  [ -n "${xen_params+set}" ] || return 0
#	  build_section xen $ARCH $(apk fetch --root "$APKROOT" --simulate xen-hypervisor | checksum)
}

profile_hyp0() {
	  profile_standard
	  profile_abbrev="hyp0"
	  title="Hypervisor Node 0"
	  desc="Built-in support for QEMU Hypervisor.
		Includes packages targetted at QEMU/KVM usage."
	  arch="x86_64"
	  kernel_cmdline="nomodeset"
    	  kernel_addons="$kernel_addons"
	  apkovl="apkovl.hyp0.sh"
	  hostname="hyp0"
	  apks="$apks ethtool lvm2 mdadm multipath-tools openvswitch rng-tools sfdisk bridge libvirt libvirt-daemon qemu-img qemu-system-x86_64 util-linux"
}
